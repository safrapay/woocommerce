<?php
/**
 * Plugin Name:       Safrapay Gateway
 * Plugin URI:        https://safrapay.com.br/
 * Description:       Gateway de pagamento do Safrapay para o WooCommerce
 * Version:           1.4.1
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Safrapay Gateway
 * Author URI:        https://safrapay.com.br
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       safrapay-gateway
 */

require_once dirname( __FILE__, 1 ) . '/vendor/autoload.php';

if ( ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	return;
}

register_activation_hook( __FILE__, function() {
	if ( ! is_plugin_active( 'woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php' ) and current_user_can( 'activate_plugins' ) ) {
		wp_die( 'Desculpe, mas este plugin requer que o plugin "Brazilian Market on WooCommerce" esteja instalado e ativo. <br><a href="' . admin_url( 'plugins.php' ) . '">&laquo; Voltar para os Plugins</a>' );
	}
} );

add_action( 'woocommerce_api_safrapay', function() { 
  $logger = wc_get_logger();
  
  $logger->info( 'Iniciando processamento do webhook', array( 'source' => 'safrapay-received-webhooks' ) );
  
  $input = json_decode(file_get_contents('php://input'), true);

  $logger->info( wc_print_r( $input, true ), array( 'source' => 'safrapay-received-webhooks' ) );
  
	if(count($input) == 0) {
		$logger->info('ID e status do pedido não encontrado na requisição', array( 'source' => 'webhook-safrapay-error' ));
		wp_die();
	}
  
	$order_id = $input['MerchantChargeId'];
	$order = wc_get_order( $order_id );
	$logger->info( 'Atualizando pedido '.$order_id, array( 'source' => 'safrapay-orders' ) );
	if( $order ){
		if( 1 == $input['ChargeStatus'] ) {
			$order->payment_complete();
			wc_reduce_stock_levels( $order_id );
		}
		else if( 2 == $input['ChargeStatus'] ) {
			$order->update_status( 'pending', __( 'Pagamento pre-autorizado.', 'wc-safrapay-card' ) );
		}
		else if( 4 == $input['ChargeStatus'] ) {
			$order->update_status( 'cancelled', __( 'Pagamento cancelado.', 'wc-safrapay-card' ) );
		}
		else { 
			$order->update_status( 'failed', __( 'Pagamento falhou.', 'wc-safrapay-card' ) );
		}
	}
	else{
		// LOG THE FAILED ORDER TO CUSTOM "failed-orders" LOG
		$logger->info( 'O pedido com o ID: ' . $input['MerchantChargeId'] . ' Não foi encontrado ', array( 'source' => 'failed-orders' ) );
  }
  
	exit();
} );

add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style( 'safrapay-style', plugin_dir_url(__FILE__) . 'assets/css/style.css', [], time() );
	wp_enqueue_script( 'jquerymask', plugin_dir_url(__FILE__) . 'assets/js/jquery.mask.js', array( 'jquery' ), time(), false );
	wp_enqueue_script( 'clipboard.js', plugin_dir_url(__FILE__) . 'assets/js/clipboard.min.js', array( 'jquery' ), time(), false );
	wp_enqueue_script( 'main-scripts', plugin_dir_url(__FILE__) . 'assets/js/app.js', array(), time(), false );
	wp_add_inline_script( 'main-scripts', "window.safrapay_plugin_url = '".plugin_dir_url(__FILE__)."'" );

	//Calculate and Pass Installments to WooCommerce Blocks
	$total = WC()->cart->get_total(null);
    $installment_options = ['' => 'Selecione a quantidade de parcelas'];
    $card = new WC_Safrapay_Card_Pay_Gateway();
    if ($total < $card->min_installments_amount) {
        $installment_options[1] = '1 parcela de R$' . number_format($total, 2, ',', '.');
    } else {
        $installment_count = $card->max_installments ? $card->max_installments : 20;
        for ($i = 1; $i <= $installment_count; $i++) {
            $installment_total = $total / $i;
            $installment_plural = ($i > 1 ? 'Parcelas' : 'Parcela');
            if ($installment_total < $card->min_installments_amount) {
                continue;
            }
            $installment_options[$i] = $i . ' ' . $installment_plural . ' de R$' . number_format($installment_total, 2, ',', '.');
        }
    }
    wp_localize_script('safrapay_card-blocks-integration', 'safrapayCardData', array(
		'installmentOptions' => $installment_options
	));
} );


/**
 * Add gateway class and register with woocommerce
 */
add_action( 'plugins_loaded', function() {
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
		return;
	}
	// ! Boleto Gateway Class Register
	include_once plugin_dir_path( __FILE__ ) . 'classes/SafrapayBoleto.class.php';
	add_filter( 'woocommerce_payment_gateways', function( $methods ) {
		$methods[] = 'WC_Safrapay_Boleto_Pay_Gateway';
		return $methods;
	}, 1000 );
	
	// ! Credit Card Gateway Class Register
	include_once plugin_dir_path( __FILE__ ) . 'classes/SafrapayCard.class.php';
	add_filter( 'woocommerce_payment_gateways', function( $methods ) {
		$methods[] = 'WC_Safrapay_Card_Pay_Gateway';
		return $methods;
	}, 1000 );
	
	// ! Pix Gateway Class Register
	include_once plugin_dir_path( __FILE__ ) . 'classes/SafrapayPix.class.php';
	add_filter( 'woocommerce_payment_gateways', function( $methods ) {
		$methods[] = 'WC_Safrapay_Pix_Pay_Gateway';
		return $methods;
	}, 1000 );
	
} , 0 );


/**
 * WooCommerce Blocks
 */
add_action('before_woocommerce_init', function() {
    if (class_exists('\Automattic\WooCommerce\Utilities\FeaturesUtil')) {
        \Automattic\WooCommerce\Utilities\FeaturesUtil::declare_compatibility('cart_checkout_blocks', __FILE__, true);
    }
});

add_action( 'woocommerce_blocks_loaded', function() {
    if ( ! class_exists( 'Automattic\WooCommerce\Blocks\Payments\Integrations\AbstractPaymentMethodType' ) ) {
        return;
    }

	// ! Pix BLocks Class Register
    require_once plugin_dir_path(__FILE__) . 'classes/blocks/PixBlock.php';
	require_once plugin_dir_path(__FILE__) . 'classes/blocks/CardBlock.php';
	require_once plugin_dir_path(__FILE__) . 'classes/blocks/BoletoBlock.php';
    add_action(
		'woocommerce_blocks_payment_method_type_registration',
		function( Automattic\WooCommerce\Blocks\Payments\PaymentMethodRegistry $payment_method_registry ) {
			$payment_method_registry->register( new WC_Safrapay_Pix_Pay_Gateway_Blocks );
			$payment_method_registry->register( new WC_Safrapay_Card_Pay_Gateway_Blocks );
			$payment_method_registry->register( new WC_Safrapay_Boleto_Pay_Gateway_Blocks );
		},
		1010
	);
});

add_action( 'woocommerce_init', 'Register_custom_checkout_fields' );
function Register_custom_checkout_fields() {

	if ( ! function_exists( 'woocommerce_register_additional_checkout_field' ) ) {
		return;
	}

	// Register CPF field
	woocommerce_register_additional_checkout_field(
		array(
			'id'       => 'safrapay/cpf',
			'label'    => 'CPF',
			'location' => 'address',
			'type'     => 'text',
			'required' => true,
		)
	);

	// Register Address Number field
	woocommerce_register_additional_checkout_field(
		array(
			'id'       => 'safrapay/address_number',
			'label'    => 'Número da Residência',
			'location' => 'address',
			'type'     => 'text',
			'required' => true,
		)
	);

	// Register Neighborhood field
	woocommerce_register_additional_checkout_field(
		array(
			'id'       => 'safrapay/neighborhood',
			'label'    => 'Bairro',
			'location' => 'address',
			'type'     => 'text',
			'required' => true,
		)
	);


	//Validate CPF
	function valida_cpf( $cpf ) {
		$cpf = preg_replace( '/[^0-9]/', '', $cpf );

		if ( strlen( $cpf ) != 11 ) {
			return false;
		}

		if ( preg_match( '/(\d)\1{10}/', $cpf ) ) {
			return false;
		}

		for ( $t = 9; $t < 11; $t++ ) {
			for ( $d = 0, $c = 0; $c < $t; $c++ ) {
				$d += $cpf[$c] * ( ( $t + 1 ) - $c );
			}
			$d = ( ( 10 * $d ) % 11 ) % 10;
			if ( $cpf[$c] != $d ) {
				return false;
			}
		}

		return true;
	}
	add_action(
		'woocommerce_validate_additional_field',
		function ( WP_Error $errors, $field_key, $field_value ) {
			if ( 'safrapay/cpf' === $field_key ) {
				$match = preg_match( '/^\d{3}\.\d{3}\.\d{3}-\d{2}$/', $field_value ) || preg_match( '/^\d{11}$/', $field_value );
				
				if ( 0 === $match || false === $match ) {
					$errors->add( 'invalid_cpf', 'Insira um CPF no formato 12345678910 ou 123.456.789-10' );
				} else if ( !valida_cpf( $field_value ) ) {
					$errors->add( 'invalid_cpf', 'Insira um CPF válido.' );
				}
			}
			return $errors;
		},
		10,
		3
	);

}

add_action('woocommerce_store_api_checkout_update_order_meta', 'save_custom_checkout_fields', 10, 1);
function save_custom_checkout_fields($order) {
    $meta_mappings = [
        '_wc_billing/safrapay/address_number' => '_billing_number',
        '_wc_billing/safrapay/neighborhood' => '_billing_neighborhood',
        '_wc_billing/safrapay/cpf' => '_billing_cpf'
    ];

    foreach ($meta_mappings as $old_key => $new_key) {
        $meta_value = $order->get_meta($old_key);

        if ($meta_value) {
            $order->update_meta_data($new_key, sanitize_text_field($meta_value));
        }
    }

    $order->save();
}


/**
 * Thank You Page Content
 *
 * @param int $order_id Order Id.
 */
add_action( 'woocommerce_thankyou', function( $order_id ) {
	$order = new WC_Order( $order_id );
	$safrapay_data = $order->get_meta( '_params_safrapay' );
	if ( $order->get_payment_method() === 'safrapay_boleto' ) {
		if ( ! empty( $safrapay_data ) ) {
			echo ('Código de Barras:');
			$generator = new Picqer\Barcode\BarcodeGeneratorHTML();
			echo ($generator->getBarcode( $safrapay_data['transaction_barcode'], $generator::TYPE_CODE_128 ));
			echo ('<p>' . $safrapay_data['transaction_digitalLine'] . '</p>');
			echo ('<div style="text-align: center">');
			if($safrapay_data['environment'] === 'sandbox')
			{
				echo ('<a href="https://payment-hml.safrapay.com.br' . $safrapay_data['transaction_bankSlipUrl'] . '" class="button button-primary download-boleto" >Clique aqui para baixar o boleto</a>');
			}else{
				echo ('<a href="https://payment.safrapay.com.br' . $safrapay_data['transaction_bankSlipUrl'] . '" class="button button-primary download-boleto">Clique aqui para baixar o boleto</a>');
			}
			echo ('</div>');
		}
	}
	else if ( $order->get_payment_method() === 'safrapay_card' ) {
		if ( ! empty( $safrapay_data ) ) {
			if($safrapay_data['transaction_transactionStatus'] === 'PreAuthorized') {
				echo ('<div class="woocommerce-info"><b>Pagamento Pré-Autorizado</b> recebemos o seu pedido mas o seu pagamento ainda não foi totalmente aprovado, assim que a compra for totalmente aprovada te notificaremos por e-mail.	</div>');
			}
			else if($safrapay_data['transaction_transactionStatus'] === 'Captured') {
				echo ('<div class="woocommerce-message"><b>Pagamento Feito!</b> recebemos o seu pagamento com sucesso.</div>');
			}
		}
	}
	else if ( $order->get_payment_method() === 'safrapay_pix' ) { 
		if ( ! empty( $safrapay_data ) ) {
			include __DIR__ . '/templates/thankyou/' . $order->get_payment_method() . '.php';
		}else{
			echo ('<div class="woocommerce-message"><b>Não foi possível carregar os dados da transação.</b> Entre em contato com o lojista.</div>');
		}
	}
} );


/**
 * Alert if checkbox not checked
 */ 


/**
 * Card Fields Checkout
 *
 * @param int $description Description.
 * @param int $payment_id  Payment Id.
 */
add_filter( 'woocommerce_gateway_description', function( $description, $payment_id ) {
	if ( 'safrapay_card' === $payment_id ) {
		$total = WC()->cart->get_total(null);
			$installment_options = ['' => 'Selecione a quantidade de parcelas'];
			$card = new WC_Safrapay_Card_Pay_Gateway();
		if($total < $card->min_installments_amount) {
			$installment_options[1] = '1 parcela de R$'.number_format($total, 2, ',', '.');
		}
		else {
			$installment_count = $card->max_installments ? $card->max_installments : 20;
			for($i = 1; $i <= $installment_count;$i++) {
				$installment_total = $total/$i;
				$installment_plural = ($i > 1 ? 'Parcelas' : 'Parcela');
				if($installment_total < $card->min_installments_amount) {
					continue;
				}
				$installment_options[$i] = $i.' '.$installment_plural.' de R$'.number_format($installment_total, 2, ',', '.');
			}
		}

		ob_start(); // ! Start buffering
		echo ('<div  class="safrapay-card-fields" style="padding:10px 0;">');
		woocommerce_form_field(
			'card_holder_name',
			array(
				'type'     => 'text',
				'label'    => __( 'Nome do Titular do Cartão', 'woocommerce' ),
				'class'    => array( 'form-row form-row-wide' ),
				'required' => true,
			),
			''
		);
		woocommerce_form_field(
			'card_holder_document',
			array(
				'type'     => 'text',
				'label'    => __( 'CPF', 'woocommerce' ),
				'class'    => array( 'form-row form-row-wide' ),
				'required' => true,
			),
			''
		);
		woocommerce_form_field(
			'safrapay_card_number',
			array(
				'type'     => 'text',
				'label'    => __( 'Número do Cartão', 'woocommerce' ),
				'class'    => array( 'form-row form-row-wide' ),
				'required' => true,
			),
			''
		);
		echo ('<span id="card-brand"></span>');
		woocommerce_form_field(
			'safrapay_card_expiration_month',
			array(
				'type'     => 'text',
				'label'    => __( 'Mês de Expiração (MM)', 'woocommerce' ),
				'class'    => array( 'form-row form-row-first' ),
				'required' => true,
				'placeholder' => 'MM',
			),
			''
		);
		woocommerce_form_field(
			'safrapay_card_year_month',
			array(
				'type'     => 'text',
				'label'    => __( 'Ano de Expiração (YY)', 'woocommerce' ),
				'class'    => array( 'form-row form-row-last' ),
				'required' => true,
				'placeholder' => 'YY',
			),
			''
		);
		woocommerce_form_field(
			'safrapay_card_cvv',
			array(
				'type'     => 'text',
				'label'    => __( 'Número de Verificação do Cartão', 'woocommerce' ),
				'class'    => array( 'form-row form-row-wide' ),
				'input_class'   => array('card_cvv'),
				'required' => true,
			),
			''
		);
		woocommerce_form_field(
			'safrapay_card_installment',
			array(
				'type'     => 'select',
				'options'  => $installment_options,
				'label'    => __( 'Quantidade de parcelas', 'woocommerce' ),
				'class'    => array( 'form-row form-row-wide installment_safrapay_card' ),
				'required' => true,
			),
			''
		);    
		echo ('</div>');
		$description .= ob_get_clean(); // ! Append buffered content
	}
	elseif( 'safrapay_boleto' === $payment_id ){
		ob_start(); // ! Start buffering
		echo ('<div  class="safrapay-pix-fields" style="padding:10px 0;">');    
		echo ('<div>');
		$description .= ob_get_clean(); // ! Append buffered content
	}
	elseif( 'safrapay_pix' === $payment_id ){
		ob_start(); // ! Start buffering   
		echo ('<div  class="safrapay-boleto-fields" style="padding:10px 0;">');   
		echo ('<div>');
		$description .= ob_get_clean(); // ! Append buffered content
	}
	return $description;
}, 20, 2 );


add_action( 'wp_ajax_get_card_brand', 'safrapay_get_card_brand' );
add_action( 'wp_ajax_nopriv_get_card_brand', 'safrapay_get_card_brand' );
/**
 * Get card number
 *
 * @param int $bin card number.
 */
function safrapay_get_card_brand() {
  if (!isset($_POST['bin'])) {
    wp_send_json( array(
      'status' => 'error',
      'brand'  => 'O número do cartão é obrigatório',
    ) );
  }
  $bin = sanitize_text_field($_POST['bin']);
  if(!$bin) {
    wp_send_json( array(
      'status' => 'error',
      'brand'  => 'O número do cartão é inválido',
    ) );
  }
	$credentials = new WC_Safrapay_Card_Pay_Gateway();
	SafrapayPayments\ApiSDK\Configuration::initialize();
	if ( 'sandbox' === $credentials->environment ) {
		SafrapayPayments\ApiSDK\Configuration::setUrl( SafrapayPayments\ApiSDK\Configuration::DEV_URL );
	}
	$merchant_numeric_cnpj = preg_replace('/[^0-9]/', '', $credentials->merchant_cnpj);
    SafrapayPayments\ApiSDK\Configuration::setCnpj($merchant_numeric_cnpj);
	SafrapayPayments\ApiSDK\Configuration::setMerchantToken( $credentials->merchant_key );
	SafrapayPayments\ApiSDK\Configuration::setlog( false );
	SafrapayPayments\ApiSDK\Configuration::login();
	$brand_name = SafrapayPayments\ApiSDK\Helper\Utils::getBrandCardBin( str_replace( ' ', '', $bin ) );
	if ( $brand_name === null ) {
		$array_result = array(
			'status' => 'error',
			'brand'  => 'null',
		);
	} else {
		if ( true === $brand_name['status'] ) {
			$array_result = array(
				'status' => 'success',
				'brand'  => $brand_name['brand'],
			);
		} else {
			$array_result = array(
				'status' => 'error',
				'brand'  => 'null',
			);
		}
	}
	wp_send_json( $array_result );
}

add_option( 'woocommerce_pay_page_id', get_option( 'woocommerce_thanks_page_id' ) );

/**
 * Update Pix Thank You Page when Paid
 */
add_action( 'wp_ajax_check_order_status', 'check_order_status' );
add_action( 'wp_ajax_nopriv_check_order_status', 'check_order_status' );

function check_order_status() {
    if ( ! isset( $_POST['order_id'] ) ) {
        wp_send_json_error( 'ID do pedido não encontrado.' );
    }

    $order_id = intval( $_POST['order_id'] );
    $order = wc_get_order( $order_id );

    if ( ! $order ) {
        wp_send_json_error( 'Pedido não encontrado.' );
    }

    wp_send_json_success( $order->get_status() );
}