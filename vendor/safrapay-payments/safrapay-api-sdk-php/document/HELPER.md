## Utils
Responsável por criar ter funcções de ajuda para a lib.

*billingInformation*

```php
SafrapayPayments\ApiSDK\Configuration::initialize();
SafrapayPayments\ApiSDK\Configuration::setUrl(SafrapayPayments\ApiSDK\Configuration::DEV_URL);
SafrapayPayments\ApiSDK\Configuration::setCnpj("83032272000109");
SafrapayPayments\ApiSDK\Configuration::setMerchantToken("mk_P1kT7Rngif1Xuylw0z96k3");
SafrapayPayments\ApiSDK\Configuration::setlog(true);
SafrapayPayments\ApiSDK\Configuration::login();

$brandName = SafrapayPayments\ApiSDK\Helper\Utils::getBrandCardBin("5463373320417272");
if ($brandName == NULL) {
    echo "Authorization::toJson = Falha ao buscar nome da bandeira do cartão\n";
    return NULL;
} else {
    echo "\n\n".$brandName."\n";
}
```
