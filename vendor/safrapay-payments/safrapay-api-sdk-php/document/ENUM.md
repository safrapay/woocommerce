# ChargeStatus

### Status da transação
```php
SafrapayPayments\ApiSDK\Enum\ChargeStatus::AUTHORIZED;        // Todas as transações da cobrança foram aprovadas.
SafrapayPayments\ApiSDK\Enum\ChargeStatus::PRE_AUTHORIZED;    // Todas as transações da cobrança foram pré-autorizadas.
SafrapayPayments\ApiSDK\Enum\ChargeStatus::CANCELED;          // Todas as transações da cobrança foram canceladas.
SafrapayPayments\ApiSDK\Enum\ChargeStatus::PARTIAL;           // As transações da cobrança diferem em status. Verificar o status de cada transação individualmente.
SafrapayPayments\ApiSDK\Enum\ChargeStatus::_AUTHORIZED;       // Todas as transações da cobrança foram negadas.
SafrapayPayments\ApiSDK\Enum\ChargeStatus::NOT_PENDING_CANCEL;// Todas as transações da cobrança estão com cancelamento pendente.
```

#

# PaymentType

###  Tipos de pagamentos
```php
SafrapayPayments\ApiSDK\Enum\PaymentType::UNDEFINED;  // Indefinido.
SafrapayPayments\ApiSDK\Enum\PaymentType::DEBIT;      // Débito.
SafrapayPayments\ApiSDK\Enum\PaymentType::CREDIT;     // Crédito.
SafrapayPayments\ApiSDK\Enum\PaymentType::VOUCHER;    // Voucher.
SafrapayPayments\ApiSDK\Enum\PaymentType::BOLETO;     // Boleto.
SafrapayPayments\ApiSDK\Enum\PaymentType::TED ;       // Transferência Eletrônica de Fundos.
SafrapayPayments\ApiSDK\Enum\PaymentType::DOC;        // Documento de Ordem de Crédito.
SafrapayPayments\ApiSDK\Enum\PaymentType::SAFETY_PAY; // SafetyPay.
```
#

# CardBrand

### Nomes das bandeiras
```php
SafrapayPayments\ApiSDK\Enum\CardBrand::VISA;
SafrapayPayments\ApiSDK\Enum\CardBrand::MASTER_CARD;
SafrapayPayments\ApiSDK\Enum\CardBrand::AMEX;
SafrapayPayments\ApiSDK\Enum\CardBrand::ELO;
SafrapayPayments\ApiSDK\Enum\CardBrand::AURA;
SafrapayPayments\ApiSDK\Enum\CardBrand::JCB;
SafrapayPayments\ApiSDK\Enum\CardBrand::DINERS;
SafrapayPayments\ApiSDK\Enum\CardBrand::DISCOVER;
SafrapayPayments\ApiSDK\Enum\CardBrand::HIPERCARD;
SafrapayPayments\ApiSDK\Enum\CardBrand::ENROUTE;
SafrapayPayments\ApiSDK\Enum\CardBrand::TICKET;
SafrapayPayments\ApiSDK\Enum\CardBrand::SODEXO;
SafrapayPayments\ApiSDK\Enum\CardBrand::VR;
SafrapayPayments\ApiSDK\Enum\CardBrand::ALELO;
SafrapayPayments\ApiSDK\Enum\CardBrand::SETRA;
SafrapayPayments\ApiSDK\Enum\CardBrand::VERO;
SafrapayPayments\ApiSDK\Enum\CardBrand::SOROCRED;
SafrapayPayments\ApiSDK\Enum\CardBrand::GREEN_CARD;
SafrapayPayments\ApiSDK\Enum\CardBrand::CABAL;
SafrapayPayments\ApiSDK\Enum\CardBrand::BANESCARD;
SafrapayPayments\ApiSDK\Enum\CardBrand::VERDE_CARD;
SafrapayPayments\ApiSDK\Enum\CardBrand::VALE_CARD;
SafrapayPayments\ApiSDK\Enum\CardBrand::UNION_PAY;
SafrapayPayments\ApiSDK\Enum\CardBrand::UP;
SafrapayPayments\ApiSDK\Enum\CardBrand::TRICARD;
SafrapayPayments\ApiSDK\Enum\CardBrand::BIGCARD;
SafrapayPayments\ApiSDK\Enum\CardBrand::BEN;
SafrapayPayments\ApiSDK\Enum\CardBrand::REDE_COMPRAS;
```

#

# AcquirerCode

### Adquirentes
```php
SafrapayPayments\ApiSDK\Enum\AcquirerCode::CIELO;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::REDE;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::STONE;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::VBI;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::GRANITO;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::INFINITE_PAY;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::SAFRA_PAY;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::ADITUM_ECOM;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::PAGSEGURO;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::ADITUM_TEF;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::SAFRAPAYTEF;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::VR_BENEFITS;
SafrapayPayments\ApiSDK\Enum\AcquirerCode::SIMULADOR;
```
#

# PhoneType

### Tipos de telefones
```php
SafrapayPayments\ApiSDK\Enum\PhoneType::RESIDENCIAL; // Telefone Residencial
SafrapayPayments\ApiSDK\Enum\PhoneType::COMERCIAL;   // Telefone Comercial
SafrapayPayments\ApiSDK\Enum\PhoneType::VOICEMAIL;   // Correio de Voz
SafrapayPayments\ApiSDK\Enum\PhoneType::TEMPORARY;   // Telefone Temporário
SafrapayPayments\ApiSDK\Enum\PhoneType::MOBILE;      // Celular
```
# DocumentType

### Tipos de documentos
```php
SafrapayPayments\ApiSDK\Enum\DocumentType::CPF;
SafrapayPayments\ApiSDK\Enum\DocumentType::CNPJ;
```

# DiscountType

### Tipos de documentos
```php
SafrapayPayments\ApiSDK\Enum\DiscountType::UNDEFINED; // Indefinido
SafrapayPayments\ApiSDK\Enum\DiscountType::PERCENTUAL;// Aplicará desconto percentual com base em uma data de pagamento.
SafrapayPayments\ApiSDK\Enum\DiscountType::FIXED;     // Aplicará um valor fixo em centavos com base na data de pagamento.
SafrapayPayments\ApiSDK\Enum\DiscountType::DAILY;     // Aplicará um valor diário em centavos com base na data de pagamento.
```