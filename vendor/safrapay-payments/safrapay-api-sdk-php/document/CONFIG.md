# Configuration
Dentro dessa classe podemos definir e obter informações como a url de desenvolvimento e produção, merchanToken e CNPJ.

```php
SafrapayPayments\ApiSDK\Configuration::initialize();

SafrapayPayments\ApiSDK\Configuration::setUrl(SafrapayPayments\ApiSDK\Configuration::DEV_URL); // Caso não defina a url, será usada de produção
SafrapayPayments\ApiSDK\Configuration::setCnpj("83032272000109");
SafrapayPayments\ApiSDK\Configuration::setMerchantToken("mk_P1kT7Rngif1Xuylw0z96k3");
SafrapayPayments\ApiSDK\Configuration::setlog(true);

print_r(SafrapayPayments\ApiSDK\Configuration::login());
```
