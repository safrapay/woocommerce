# Authentication
A autenticação é a forma que deve ser utilizada para gerar um Token de acesso, para que você consiga utilizar os endpoints.

```php
SafrapayPayments\ApiSDK\Configuration::initialize();

SafrapayPayments\ApiSDK\Configuration::setUrl(SafrapayPayments\ApiSDK\Configuration::DEV_URL); // Caso não defina a url, será usada de produção
SafrapayPayments\ApiSDK\Configuration::setCnpj("83032272000109");
SafrapayPayments\ApiSDK\Configuration::setMerchantToken("mk_P1kT7Rngif1Xuylw0z96k3");
SafrapayPayments\ApiSDK\Configuration::setlog(true);

$res = SafrapayPayments\ApiSDK\Configuration::login();

if (isset($res["token"])) {
	echo  $res["token"]."\n";
	
} else {
	echo  "httStatus: ".$res["httpStatus"]
		."\n httpMsg: ".$res["httpMsg"]
		."\n";
}
```
