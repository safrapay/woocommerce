=== SafraPay Gateway ===
Tags: woocommerce, SafraPay, payment
Requires at least: 4.0
Tested up to: 6.4.2
Stable tag: 1.4.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Contributors: pluginssafrapay
Requires PHP: 7.2

Adds SafraPay gateway to the WooCommerce plugin

== Description ==

### Add SafraPay gateway to WooCommerce ###

This plugin adds SafraPay gateway to WooCommerce.

Please notice that WooCommerce must be installed and active.


### Descrição em Português: ###

Adicione o SafraPay como método de pagamento em sua loja WooCommerce.

[SafraPay](https://safrapay.com.br/) é um método de pagamento brasileiro.

O plugin WooCommerce SafraPay foi desenvolvido para integração de pagamento da sua loja virtual woocommerce com o nosso gateway com checkout transparente.

Estão disponíveis as seguintes modalidades de pagamento:

- **Cartão de Crédito:**
- **Boleto Bancário:**
- **Pix:**


= Compatibilidade =

Compatível com versões posteriores ao WooCommerce 3.0.

Este plugin também é compatível com o [WooCommerce Extra Checkout Fields for Brazil](https://wordpress.org/plugins/woocommerce-extra-checkout-fields-for-brazil/), desta forma é possível enviar os campos de "CPF", "número do endereço" e "bairro" (para o Checkout Transparente é obrigatório o uso deste plugin).

= Instalação =

Confira o nosso [passo a passo de instalação e configuração](https://github.com/safrapay-payments/safrapay-woocommerce).

= Integração =

Este plugin funciona perfeitamente em conjunto com:

* [WooCommerce Extra Checkout Fields for Brazil](https://wordpress.org/plugins/woocommerce-extra-checkout-fields-for-brazil/).
* [WooCommerce Multilingual](https://wordpress.org/plugins/woocommerce-multilingual/).

= Dúvidas? =

Para dúvidas podem acessar o link abaixo:
https://safrapay.com.br



== Installation ==

* Upload plugin files to your plugins folder, or install using WordPress built-in Add New Plugin installer;
* Activate the plugin;
* Navigate to WooCommerce -> Settings -> Payment Gateways, choose SafraPay and fill in your CNPJ and Merchant Token:

### Instalação e configuração em Português: ###

= Instalação do plugin: =

* Envie os arquivos do plugin para a pasta wp-content/plugins, ou instale usando o instalador de plugins do WordPress.
* Ative o plugin.

= Requerimentos: =

É necessário possuir uma conta no [SafraPay](https://safrapay.com.br/) e ter instalado o [WooCommerce](https://wordpress.org/plugins/woocommerce/).

Apenas com isso já é possível receber os pagamentos e fazer o retorno automático de dados.

<blockquote>Atenção: Não é necessário configurar qualquer URL em "Página de redirecionamento" ou "Notificação de transação", pois o plugin é capaz de comunicar com api SafraPay.</blockquote>

= Configurações do Plugin: =

Com o plugin instalado acesse o admin do WordPress e entre em "WooCommerce" > "Configurações" > "Finalizar compra" > "SafraPay".

Habilite o SafraPay, adicione o seu CNPJ e o Merchant Token:. Esses dados são utilizado para gerar os pagamentos e fazer o retorno de dados.

Você pode conseguir o seu Merchant Token no sua conta no site da SafraPay" > "(https://safrapay.com.br)".


= Checkout Transparente =
Para utilizar o checkout transparente é necessário utilizar o plugin 


Pronto, sua loja já pode receber pagamentos pelo SafraPay.

== Frequently Asked Questions ==

= What is the plugin license? =

* This plugin is released under a GPL license.

= What is needed to use this plugin? =

* WooCommerce version 3.0 or latter installed and active.
* Only one account on [SafraPay](https://safrapay.com.br/ "SafraPay").

### FAQ em Português: ###

= Qual é a licença do plugin? =

Este plugin esta licenciado como GPL.

= O que eu preciso para utilizar este plugin? =

* Ter instalado o plugin WooCommerce 3.0 ou mais recente.
* Possuir uma conta no SafraPay.
* Gerar seu CNPJ e o Merchant Token da SafraPay.


= Adimtum recebe pagamentos de quais países? =

No momento a SafraPay recebe pagamentos apenas do Brasil.

Configuramos o plugin para receber pagamentos apenas de usuários que selecionarem o Brasil nas informações de pagamento durante o checkout.

= Quais são os meios de pagamento que o plugin aceita? =

São aceitos todos os meios de pagamentos que a SafraPay disponibiliza.

Confira os [meios de pagamento e parcelamento](https://safrapay.com.br).


== Changelog ==

= SafraPay Gateway 1.4.1 =
* Fix issues with billing number

= SafraPay Gateway 1.4.0 =
* Fix issues with Pix and wordpress.org

= SafraPay Gateway 1.3.6 =
* Bug fix

= SafraPay Gateway 1.3.5 =
* Bug fix

= SafraPay Gateway 1.3.4 =
* Bug fix

= SafraPay Gateway 1.3.3 =
* Bug fix

= SafraPay Gateway 1.3.2 =
* Bug fix

= SafraPay Gateway 1.3.1 =
* Bug fix

= SafraPay Gateway 1.3.0 =
* Adição de opção de debug

= SafraPay Gateway 1.2.1 =
* Bug fix

= SafraPay Gateway 1.2.0 =
* Envio de dados dos itens do pedido para a API

= SafraPay Gateway 1.1.1 =
* Atualização da documentação

= SafraPay Gateway 1.1.0 =
* Adicionado método de pagamento por Pix

= SafraPay Gateway 1.0.4 =
* Ajuste antifraude

= SafraPay Gateway 1.0.3 =
* Correção de bugs

= SafraPay Gateway 1.0.2 =
* Correção de bugs

= SafraPay Gateway 1.0.1 =
* Atualizações de informações

= SafraPay Gateway 1.0.0 =
* Plublicação do plugin

= SafraPay Gateway =
* Atualização dos screenshots

== Upgrade Notice ==

== Screenshots ==
