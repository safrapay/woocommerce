<div class="pix-thankyou">
    <div class="codigo-qr">
        <h3>Pagar com código Copia e Cola:</h3>
        <div class="pix">
            <input  id="codigo" type="text" value="<?php echo $safrapay_data['qrCode']; ?>">
            <button class="btn" data-clipboard-target="#codigo">
                <img width="30" src="<?php echo plugin_dir_url(dirname(__DIR__)); ?>assets/clippy.svg" alt="Copiar código">
            </button>
        </div>
        <div id="msg-copy"></div>
        <script>
            var clipboard = new ClipboardJS('.btn');
            clipboard.on('success', function(e) {
                e.clearSelection();
                var msg = document.querySelector('#msg-copy');
                msg.innerHTML = 'Código copiado!'
                setTimeout(() => msg.innerHTML = '', 3000)
            });

        </script>
        <div class="alert alert-error">O código Pix é válido por 30 minutos</div>
    </div>
    <br/>
    <br/>
    <div class="image-qr">  
        <h3>Pagar com QR Code:</h3>
        <div class="pix-qr">
            <img width="300" src="data:image/jpeg;base64,<?php echo $safrapay_data['qrCodeBase64']; ?>">
        </div>
        <div class="alert alert-error">O QR Code é válido por 30 minutos</div>      
    </div>
    <br/>
    <br/>

    <div id="order-status-message" class="order-status-message">
        <span id="order-status-icon" class="order-status-icon loading">⏳</span>
        <span id="order-status-text">Aguardando o pagamento...</span>
    </div>

    <style>
        .order-status-message {
            display: flex;
            align-items: center;
            background-color: #f1f1f1;
            padding: 15px 30px;
            border-radius: 8px;
            color: #333;
            width: fit-content;
        }

        .order-status-icon  {
            margin-right: 10px;
            transition: transform 0.3s ease-in-out;
        }

        .order-status-processing {
            background-color: #c3e6cb;
            color: #155724;
        }

        .order-status-cancelled {
            background-color: #f5c6cb;
            color: #721c24;
        }

        .loading {
            display: inline-block;
            animation: spin 2s ease infinite;
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>

    <script>
        var orderId = <?php echo $order->get_id(); ?>;
        
        function updateOrderStatusUI(status) {
            var statusMessage = document.getElementById('order-status-message');
            var statusText = document.getElementById('order-status-text');
            var statusIcon = document.getElementById('order-status-icon');

            if (status === 'processing') {
                statusMessage.className = 'order-status-message order-status-processing';
                statusText.innerHTML = 'Pagamento confirmado. Seu pedido está sendo processado!';
                statusIcon.innerHTML = '✅';
                statusIcon.classList.remove('loading');
                clearInterval(checkStatusInterval);
            } else if (status === 'failed') {
                statusMessage.className = 'order-status-message order-status-cancelled';
                statusText.innerHTML = 'O pagamento falhou. Por favor, tente novamente.';
                statusIcon.innerHTML = '❌';
                statusIcon.classList.remove('loading');
                clearInterval(checkStatusInterval);
            } else if (status === 'cancelled') {
                statusMessage.className = 'order-status-message order-status-cancelled';
                statusText.innerHTML = 'O pagamento foi cancelado.';
                statusIcon.innerHTML = '❌';
                statusIcon.classList.remove('loading');
                clearInterval(checkStatusInterval);
            }
        }

        function checkOrderStatus() {
            jQuery.ajax({
                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'check_order_status',
                    order_id: orderId
                },
                success: function(response) {
                    if (response.success) {
                        updateOrderStatusUI(response.data);
                    } else {
                        console.error('Erro ao verificar o status do pedido:', response.data);
                    }
                }
            });
        }

        var checkStatusInterval = setInterval(checkOrderStatus, 5000);
    </script>
</div>