# Instalação do plugin WooCommerce Safrapay

### Requisitos

É necessário instalar o plugin **Extra Checkout Fields for Brazil** para ser exibido o campo de CPF/CNPJ caso esteja utilizando um tema legado.

https://wordpress.org/plugins/woocommerce-extra-checkout-fields-for-brazil

### Baixar plugin
Na tela administrativa do wordpress, seguir o caminho:
```
Plugins > Add New Plugin
```
E pesquise por "SafraPay Gateway", clique em ```Install Now``` e ```Activate```.

### Configurar o plugin
Na tela administrativa do wordpress, seguir o caminho:
```
WooCommerce > Settings > Payments
```
Ao chegar no final da página você deve encontrar algumas opções de pagamento do plugin Safrapay, ative as opções que deseja utilizar em seu checkout e clique em ```Manage``` para configurá-las

### Safrapay Cartão de Crédito
- **Habilitar/Desabilitar** `Yes/No`
  *Habilita o módulo para ser exibido no checkout*

- **Habilitar debug:** `Yes/No`
  *Habilita o modo de debug do plugin*

- **Ambiente do Gateway:** `Produção/Sandbox`
  *Seleciona entre fazer transação em produção ou em ambiente de homologação(sandbox)*

- **Título do Gateway:** `Título que aparecerá no checkout`

- **Descrição do Gateway:** `Descrição que aparecerá abaixo do título no chekcout`

- **Instruções Após o Pedido:** `Texto que aparecerá na página de obrigado e no e-mail do pedido`

- **Valor mínimo da parcela** `valor mínimo de uma parcela em R$`

- **Número máximo de parcelas:** `mínimo de 1 e máximo de 12`

- **Tempo de expiração do Pedido:** `Tempo que o link pedido estará disponivel para pagamento`

- **Status do Pedido criado:** `Pending payment/Processing/On hold/Completed/Cancelled/Refunded/Failed/Draft`
*Status inicial para novos pedidos*

- **CNPJ** `número do cnpj`
*Deve ser colocado o cnpj cadastrado na Safrapay*

- **Merchant Token** `id do merchant`
*ID do merchant da Safrapay*

###  Safrapay Boleto
- **Habilitar/Desabilitar** `Yes/No`
  *Habilita o módulo para ser exibido no checkout*

- **Habilitar debug:** `Yes/No`
  *Habilita o modo de debug do plugin*

- **Ambiente do Gateway:** `Produção/Sandbox`
  *Seleciona entre fazer transação em produção ou em ambiente de homologação(sandbox)*

- **Título do Gateway:** `Título que aparecerá no checkout`

- **Descrição do Gateway:** `Descrição que aparecerá abaixo do título no chekcout`

- **Instruções Após o Pedido:** `Texto que aparecerá na página de obrigado e no e-mail do pedido`

- **Tempo de expiração do Pedido:** `Tempo que o link pedido estará disponivel para pagamento`

- **Tempo de expiração do boleto (Dias):** `Validade do boleto`

- **Dias para multa** `número de dias` 
*Dias para começar aplicar multa por atraso*

- **Valor fixo da multa**  `valor em reais a ser pago na multa`

- **Valor percentual da multa**  `valor percentual a ser pago na multa`

- **Status do Pedido criado:** `Pending payment/Processing/On hold/Completed/Cancelled/Refunded/Failed/Draft`
*Status inicial para novos pedidos*

- **CNPJ** `número do cnpj`
*Deve ser colocado o cnpj cadastrado na Safrapay*

- **Merchant Token** `id do merchant`
*ID do merchant da Safrapay*

###  Safrapay Pix
- **Habilitar/Desabilitar** `Yes/No`
  *Habilita o módulo para ser exibido no checkout*

- **Habilitar debug:** `Yes/No`
  *Habilita o modo de debug do plugin*

- **Ambiente do Gateway:** `Produção/Sandbox`
  *Seleciona entre fazer transação em produção ou em ambiente de homologação(sandbox)*

- **Título do Gateway:** `Título que aparecerá no checkout`

- **Descrição do Gateway:** `Descrição que aparecerá abaixo do título no chekcout`

- **Instruções Após o Pedido:** `Texto que aparecerá na página de obrigado e no e-mail do pedido`

- **Status do Pedido criado:** `Pending payment/Processing/On hold/Completed/Cancelled/Refunded/Failed/Draft`
*Status inicial para novos pedidos*

- **CNPJ** `número do cnpj`
*Deve ser colocado o cnpj cadastrado na Safrapay*

- **Merchant Token** `id do merchant`
*ID do merchant da Safrapay*

### Registrar Webhook

Agora para que o plugin possa se comunicar com o gateway Safrapay, iremos precisar registrar o webhook, como no exemplo abaixo:

```
POST 
{{ENDPOINT_WEBHOOK}}/v1/webhook/bulk

Body:
{
   "email": "seuemail@email.com.br",
   "webhooks": [
      {
         "targetUrl": "https://seudomínio/wc-api/safrapay",
         "eventType": 1
      },
      {
         "targetUrl": "https://seudomínio/wc-api/safrapay",
         "eventType": 2
      }
   ]
}
```

Obs: Autenticação para utilização da API será feita no endpoint {{ENDPOINT_GATEWAY}}/v2/merchant/auth