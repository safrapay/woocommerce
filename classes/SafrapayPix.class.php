<?php
/**
 * Safrapay Gateway Payment Pix Class
 * Description: Pix Class
 *
 * @package Safrapay/Payments
 */

/**
 * Class Init WooCommerce Gateway
 */
class WC_Safrapay_Pix_Pay_Gateway extends WC_Payment_Gateway {

	/**
	 * Whether or not logging is enabled
	 *
	 * @var bool
	 */
	public static $log_enabled = true;

	/**
	 * Logger instance
	 *
	 * @var WC_Logger
	 */
	public static $log = true;

	/**
	 * Merchant Key Credentials
	 *
	 * @var string
	 */
	public $merchant_key = '';

	/**
	 * Merchant CNPJ Credentials
	 *
	 * @var string
	 */
	public $merchant_cnpj = '';

	/**
	 * Ambient Environment
	 *
	 * @var string
	 */
	public $environment = '';

	/**
	 * Ambient Deadline
	 *
	 * @var string
	 */
	public $deadline = 0;


	/**
	 * Ambient Initial Status
	 *
	 * @var string
	 */
	public $initial_status = '';

	/**
	 * Ambient Expiry Date
	 *
	 * @var string
	 */
	public $expiry_date = '';

	/**
	 * Ambient Max Installment
	 *
	 * @var string
	 */
	public $max_installment = '';

	/**
	 * Instructions for the payment method
	 *
	 * @var string
	 */
	public $instructions = '';

	/**
	 * Debug mode
	 *
	 * @var bool
	 */
	public $debug = false;

	/**
	 * Function Plugin constructor
	 */
	public function __construct() {
		$this->id = 'safrapay_pix';
		// $this->icon               = apply_filters( 'woocommerce_safrapay_pix_icon', plugins_url() . '/../plugins/safrapay-boleto-gateway/assets/icon.png' );
		$this->has_fields         = true;
		$this->method_title       = __( 'Safrapay Pix', 'wc-safrapay-pix' );
		$this->method_description = __( 'Safrapay Pagamento por Pix', 'wc-safrapay-pix' );

		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option(
			'instructions',
			$this->description
		);

		$this->supports = array(
			'products',
		);

		$this->merchant_key   = $this->get_option( 'safrapay_pix_merchantKey' );
		$this->merchant_cnpj  = $this->get_option( 'safrapay_pix_cnpj' );
		$this->environment    = $this->get_option( 'safrapay_pix_environment' );
		$this->debug          = $this->get_option( 'safrapay_pix_debug' );
		$this->initial_status = $this->get_option( 'safrapay_pix_initial_status' );

		$this->init_form_fields();
		$this->init_settings();

		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_thank_you_' . $this->id, array( $this, 'thankyou_page' ) );
		
	}

	/**
	 * Init init_form_fields form fields
	 */
	public function init_form_fields() {

		$this->form_fields = apply_filters(
			'woo_safrapay_pix_pay_fields',
			array(
				'enabled'                    => array(
					'title'   => __( 'Habilitar/Desabilitar:', 'wc-safrapay_pix' ),
					'type'    => 'checkbox',
					'label'   => __( 'Habilitar ou desabilitar o Módulo de Pagamento', 'wc-safrapay_pix' ),
					'default' => 'no',
				),
				'safrapay_pix_debug'           => array(
					'title'   => __( 'Habilitar debug:', 'wc-safrapay_pix' ),
					'type'    => 'checkbox',
					'label'   => __( 'Habilitar ou desabilitar debug', 'wc-safrapay_pix' ),
					'default' => 'no',
				),
				'safrapay_pix_environment'    => array(
					'title'   => __( 'Ambiente do Gateway:', 'wc-safrapay_pix' ),
					'type'    => 'select',
					'options' => array(
						'production' => __( 'Produção', 'wc-safrapay_pix' ),
						'sandbox'    => __( 'Sandbox', 'wc-safrapay_pix' ),
					),
				),
				'title'                      => array(
					'title'       => __( 'Título do Gateway:', 'wc-safrapay_pix' ),
					'type'        => 'text',
					'description' => __( 'Adicione um novo título ao safrapay, os clientes vão visualizar ese título no checkout.', 'wc-safrapay_pix' ),
					'default'     => __( 'Safrapay Gateway - Pix', 'wc-safrapay_pix' ),
					'desc_tip'    => true,
				),
				'description'                => array(
					'title'       => __( 'Descrição do Gateway:', 'wc-safrapay_pix' ),
					'type'        => 'textarea',
					'description' => __( 'Adicione uma nova descrição para o safrapay.', 'wc-safrapay_pix' ),
					'default'     => __( 'Pague com total segurança através do Pix.', 'wc-safrapay_pix' ),
					'desc_tip'    => true,
				),
				'instructions'               => array(
					'title'       => __( 'Instruções Após o Pedido:', 'wc-safrapay_pix' ),
					'type'        => 'textarea',
					'description' => __( 'As instruções iram aparecer na página de Obrigado & Email após o pedido ser feito.', 'wc-safrapay_pix' ),
					'desc_tip'    => true,
				),
				'safrapay_pix_initial_status' => array(
					'title'       => __( 'Status do Pedido criado:', 'wc-safrapay_pix' ),
					'type'        => 'select',
					'options'     => wc_get_order_statuses(),
					'description' => __( 'Status do pedido criado.', 'wc-safrapay_pix' ),
					'desc_tip'    => true,
					'default'     => 'wc-pending'
				),
				'safrapay_pix_cnpj'           => array(
					'title'       => __( 'CNPJ:', 'wc-safrapay_pix' ),
					'type'        => 'text',
					'description' => __( 'Insira o CNPJ cadastrado no Safrapay.', 'wc-safrapay_pix' ),
					'desc_tip'    => true,
				),
				'safrapay_pix_merchantKey'    => array(
					'title'       => __( 'Merchant Token:', 'wc-safrapay_pix' ),
					'type'        => 'text',
					'description' => __( 'Insira o Merchant Key cadastrado no Safrapay.', 'wc-safrapay_pix' ),
					'desc_tip'    => true,
				)
			)
		);
	}
	

	/**
	 * Logging method.
	 *
	 * @param string $message Log message.
	 * @param string $level Optional. Default 'info'. Possible values:
	 *                      emergency|alert|critical|error|warning|notice|info|debug.
	 */
	public static function log( $message, $level = 'info' ) {
		if ( self::$log_enabled ) {
			if ( empty( self::$log ) ) {
				self::$log = wc_get_logger();
			}
			self::$log->log( $level, $message, array( 'source' => 'wc-safrapay-pix' ) );
		}
	}

	public function validateInputs( $data ) {

		$keys = array(
		);

		foreach ( $data as $key => $input ) {
			if ( in_array( $key, $keys ) ) {
				if ( empty( $data[ $key ] ) ) {
					return false;
				}
			}
		}

		return true;

	}
	/**
	 * Process_payment method.
	 *
	 * @param int $order_id Id of order.
	 */
	public function process_payment( $order_id ) {

		global $woocommerce;
		$order = new WC_Order( $order_id );

		$address_1    = $order->get_meta( '_billing_address_1' );
        $address_2    = $order->get_meta( '_billing_address_2' );
        $address_number =  $order->get_meta( '_billing_number' );
        $address_neightboorhood = $order->get_meta( '_billing_neighborhood' );

		SafrapayPayments\ApiSDK\Configuration::initialize();
		if ( 'sandbox' === $this->environment ) {
			SafrapayPayments\ApiSDK\Configuration::setUrl( SafrapayPayments\ApiSDK\Configuration::DEV_URL );
		}
		// wp_send_json([$this->merchant_cnpj, $this->merchant_key]);
		$merchant_numeric_cnpj = preg_replace('/[^0-9]/', '', $this->merchant_cnpj);
        SafrapayPayments\ApiSDK\Configuration::setCnpj($merchant_numeric_cnpj);
		SafrapayPayments\ApiSDK\Configuration::setMerchantToken( $this->merchant_key );
		SafrapayPayments\ApiSDK\Configuration::setlog( false );
		SafrapayPayments\ApiSDK\Configuration::login();

		if ( ! $this->validateInputs( $_POST ) ) {
			return wc_add_notice( 'Preencha todos os campos.', 'error' );
		}

		$gateway       = new SafrapayPayments\ApiSDK\Gateway();
		$pix = new SafrapayPayments\ApiSDK\Domains\Pix;

		foreach($order->get_items() as $item) {
			$product = new WC_Product($item->get_product_id());
			$total_product_amount = preg_replace( '/[^0-9]/', '', number_format($item->get_subtotal(),2));
			$product_amount = round($total_product_amount / $item->get_quantity(), 0);
			$pix->products->add(
				// name
				$item->get_name(),  
				// sku
				$product->get_sku(), 
				// price
				$product_amount,
				// quantity
				$item->get_quantity()
			);
		}

		$phone = preg_replace( '/[^\d]+/', '', $order->get_billing_phone() );

		$customer_phone_area_code = substr( $phone, 0, 2 );
		$customer_phone           = substr( $phone, 2 );
		$amount                   = str_replace( '.', '', $order->get_total() );

		$pix->setMerchantChargeId($order_id);

		// ! Customer
		$pix->customer->setName( $order->get_billing_first_name() . ' ' . $order->get_billing_last_name() );
		$pix->customer->setEmail( $order->get_billing_email() );
		$pix->customer->setId( "$order_id" );

		if (!empty($order->get_meta('_billing_cpf'))) {
            $pix->customer->setDocumentType(SafrapayPayments\ApiSDK\Enum\DocumentType::CPF);
            $documento = preg_replace('/[^\d]+/', '', $order->get_meta('_billing_cpf'));
        } else if (!empty($order->get_meta('_billing_cnpj'))) {
            $pix->customer->setDocumentType(SafrapayPayments\ApiSDK\Enum\DocumentType::CNPJ);
            $documento = preg_replace('/[^\d]+/', '', $order->get_meta('_billing_cnpj'));
        }
		
		$pix->customer->setDocument( $documento );

		// ! Customer->address
		$pix->customer->address->setStreet( $address_1 );
		$pix->customer->address->setNumber( $address_number  );
		$pix->customer->address->setNeighborhood( $address_neightboorhood );
		$pix->customer->address->setCity( $order->get_billing_city() );
		$pix->customer->address->setState( $order->get_billing_state() );
		$pix->customer->address->setCountry( $order->get_billing_country() );
		$pix->customer->address->setZipcode( str_replace( '-', '', $order->get_billing_postcode() ) );
		$pix->customer->address->setComplement( $address_2 );

		// ! Customer->phone
		$pix->customer->phone->setCountryCode( '55' );
		$pix->customer->phone->setAreaCode( $customer_phone_area_code );
		$pix->customer->phone->setNumber( $customer_phone );
		$pix->customer->phone->setType( SafrapayPayments\ApiSDK\Enum\PhoneType::MOBILE );

		// ! Transactions
		$pix->transactions->setAmount( $amount );

		$res = $gateway->charge( $pix );
		
		if($this->debug == 'yes') {
			wc_get_logger()->info( wc_print_r( $res, true ), array( 'source' => 'safrapay-pix-orders-response' ) );
		}

		if ( isset( $res['status'] ) ) {

			//Compress QrCode
			$qrCodeImg = base64_decode($res['charge']->transactions[0]->qrCodeBase64);
			ob_start();
			imagejpeg (imagecreatefromstring($qrCodeImg));
			$compressedQrCodeImg = ob_get_contents();
			ob_end_clean();

			$order->update_meta_data(
				'_params_safrapay',
				array(
					'order_id'                     => $order_id,
					'chargeId'                => $res['charge']->id,
					'chargeStatus'            => $res['charge']->chargeStatus,
					'qrCode'          => $res['charge']->transactions[0]->qrCode,
					'qrCodeBase64'          => base64_encode($compressedQrCodeImg),
					'transaction_id'          => $res['charge']->transactions[0]->transactionId,
					'transaction_amount'      => $res['charge']->transactions[0]->amount,
					'transaction_transactionStatus' => $res['charge']->transactions[0]->transactionStatus,
				)
			);

			$order->save();

			if ( SafrapayPayments\ApiSDK\Enum\ChargeStatus::NOT_AUTHORIZED === $res['status'] ) {
				$order->update_status( 'failed', __( 'Pagamento Não Autorizado', 'wc-safrapay-pix' ) );

				if($this->debug == 'yes') {
					wc_add_notice( json_encode($res) , 'error' );
				}
				wc_add_notice( 'Transação não autorizada.', 'error' );
				return [
					'result'   => 'failure',
					'message'  => __( 'Transação não autorizada.', 'wc-safrapay' )
				];
			}

			else if ( SafrapayPayments\ApiSDK\Enum\ChargeStatus::PRE_AUTHORIZED === $res['status'] ) {
				$order->update_status( $this->initial_status, __( 'Aguardando o pagamento do pix', 'wc-safrapay-pix' ) );
				
				// ! Remove cart
				$woocommerce->cart->empty_cart();
				
				// ! Return thankyou redirect
				return array(
					'result'   => 'success',
					'redirect' => $this->get_return_url( $order ),
				);
			}

			else if ( SafrapayPayments\ApiSDK\Enum\ChargeStatus::AUTHORIZED === $res['status'] ) {
				$order->update_status( 'processing', __( 'Pagamento Concluído', 'wc-safrapay-pix' ) );

				$woocommerce->cart->empty_cart();

				return [
					'result'   => 'success',
					'redirect' => $this->get_return_url( $order ),
				];
			} 
			
			else {
				$order->update_status( 'failed', __( 'Pagamento Falhou', 'wc-safrapay-pix' ) );

				if($this->debug == 'yes') {
					wc_add_notice( json_encode($res) , 'error' );
				}
				$errorMessage = $charge->transactions[0]->errorMessage ?? 'Erro desconhecido.';
				wc_add_notice($errorMessage, 'error');
				return [
					'result'   => 'failure',
					'message'  => __( 'Erro desconhecido.', 'wc-safrapay' )
				];
			}
		} else {

			$order->update_status( 'failed', __( 'Pagamento Falhou', 'wc-safrapay-pix' ) );

			if ( null !== $res ) {
				if($this->debug == 'yes') {
					return wc_add_notice( json_encode($res) , 'error' );
				}

				if($res['httpMsg'] === ''){
					return wc_add_notice( 'Verifique as credenciais de acesso ao Safrapay e tente novamente. <br/>' . json_encode($res), 'error' );
				}

				$messages = json_decode($res['httpMsg'], true);
				$erros = '';
				foreach($messages['errors'] as $errors){
					$erros .= $errors['message'].'<br>';
				}
				return [
					'result' => 'failure',
					'message' => __($erros, 'wc-safrapay')
				];
			}
		}
	}

	/**
	 * Thankyou_page method.
	 */
	public function thankyou_page() {
		if ( $this->instructions ) {
			echo wp_kses_post( wpautop( wptexturize( $this->instructions ) ) );
		}
	}
}
