<?php
/**
 * Safrapay Gateway Payment Boleto Class
 * Description: Boleto Class
 *
 * @package Safrapay/Payments
 */

/**
 * Class Init WooCommerce Gateway
 */
class WC_Safrapay_Boleto_Pay_Gateway extends WC_Payment_Gateway {

	/**
	 * Whether or not logging is enabled
	 *
	 * @var bool
	 */
	public static $log_enabled = true;

	/**
	 * Logger instance
	 *
	 * @var WC_Logger
	 */
	public static $log = true;

	/**
	 * Merchant Key Credentials
	 *
	 * @var string
	 */
	public $merchant_key = '';

	/**
	 * Merchant CNPJ Credentials
	 *
	 * @var string
	 */
	public $merchant_cnpj = '';

	/**
	 * Ambient Environment
	 *
	 * @var string
	 */
	public $environment = '';

	/**
	 * Ambient Deadline
	 *
	 * @var string
	 */
	public $deadline = 0;


	/**
	 * Ambient Initial Status
	 *
	 * @var string
	 */
	public $initial_status = '';

	/**
	 * Ambient Initial Status
	 *
	 * @var string
	 */
	public $fine_start = '';

	/**
	 * Ambient Initial Status
	 *
	 * @var string
	 */
	public $fine_value = '';

	/**
	 * Ambient Initial Status
	 *
	 * @var string
	 */
	public $fine_percentual = '';

	/**
	 * Ambient Expiry Date
	 *
	 * @var string
	 */
	public $expiry_date = '';

	/**
	 * Instructions for the payment method
	 *
	 * @var string
	 */
	public $instructions = '';

	/**
	 * Debug mode
	 *
	 * @var bool
	 */
	public $debug = false;


	/**
	 * Function Plugin constructor
	 */
	public function __construct() {
		$this->id                 = 'safrapay_boleto';
		$this->icon               = apply_filters( 'woocommerce_safrapay_boleto_icon', plugin_dir_url(__DIR__) . 'assets/icon.png' );
		$this->has_fields         = true;
		$this->method_title       = __( 'Safrapay Boleto', 'wc-safrapay-boleto' );
		$this->method_description = __( 'Safrapay Pagamento por Boleto', 'wc-safrapay-boleto' );

		$this->title        = $this->get_option( 'title' );
		$this->description  = $this->get_option( 'description' );
		$this->instructions = $this->get_option(
			'instructions',
			$this->description
		);

		$this->supports = array(
			'products',
		);

		$this->merchant_key   = $this->get_option( 'safrapay_boleto_merchantKey' );
		$this->merchant_cnpj  = $this->get_option( 'safrapay_boleto_cnpj' );
		$this->environment    = $this->get_option( 'safrapay_boleto_environment' );
		$this->debug          = $this->get_option( 'safrapay_boleto_debug' );
		$this->initial_status = $this->get_option( 'safrapay_boleto_initial_status' );
		$this->deadline       = $this->get_option( 'safrapay_boleto_deadline_boleto' );

		// Fines
		$this->fine_start       	 = $this->get_option( 'safrapay_boleto_fine_start' );
		$this->fine_value      		 = $this->get_option( 'safrapay_boleto_fine_value' );
		$this->fine_percentual       = $this->get_option( 'safrapay_boleto_fine_percentual' );

		$this->expiry_date = $this->get_option( 'safrapay_boleto_order_expiry' );


		$this->init_form_fields();
		$this->init_settings();

		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		add_action( 'woocommerce_thank_you_' . $this->id, array( $this, 'thankyou_page' ) );
	}
	/**
	 * Init init_form_fields form fields
	 */
	public function init_form_fields() {

		$this->form_fields = apply_filters(
			'woo_safrapay_boleto_pay_fields',
			array(
				'enabled'                       => array(
					'title'   => __( 'Habilitar/Desabilitar:', 'wc-safrapay-boleto' ),
					'type'    => 'checkbox',
					'label'   => __( 'Habilitar ou desabilitar o Módulo de Pagamento', 'wc-safrapay-boleto' ),
					'default' => 'no',
				),
				'safrapay_boleto_debug'           => array(
					'title'   => __( 'Habilitar debug:', 'wc-safrapay-boleto' ),
					'type'    => 'checkbox',
					'label'   => __( 'Habilitar ou desabilitar o debug', 'wc-safrapay-boleto' ),
					'default' => 'no',
				),
				'safrapay_boleto_environment'     => array(
					'title'   => __( 'Ambiente do Gateway:', 'wc-safrapay-boleto' ),
					'type'    => 'select',
					'options' => array(
						'production' => __( 'Produção', 'wc-safrapay-boleto' ),
						'sandbox'    => __( 'Sandbox', 'wc-safrapay-boleto' ),
					),
				),
				'title'                         => array(
					'title'       => __( 'Título do Gateway:', 'wc-safrapay-boleto' ),
					'type'        => 'text',
					'description' => __( 'Adicione um novo título ao safrapay Boleto Gateway, os clientes vão visualizar ese título no checkout.', 'wc-safrapay-boleto' ),
					'default'     => __( 'Safrapay Gateway - Boleto', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'description'                   => array(
					'title'       => __( 'Descrição do Gateway:', 'wc-safrapay-boleto' ),
					'type'        => 'textarea',
					'description' => __( 'Adicione uma nova descrição para o safrapay Boleto Gateway.', 'wc-safrapay-boleto' ),
					'default'     => __( 'Pague com total segurança através do boleto bancário.', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'instructions'                  => array(
					'title'       => __( 'Instruções Após o Pedido:', 'wc-safrapay-boleto' ),
					'type'        => 'textarea',
					'description' => __( 'As instruções iram aparecer na página de Obrigado & Email após o pedido ser feito.', 'wc-safrapay-boleto' ),
					'default'     => __( '', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'safrapay_boleto_order_expiry' => array(
					'title'       => __( 'Tempo de expiração do Pedido:', 'wc-safrapay_card' ),
					'type'        => 'number',
					'description' => __( 'Depois de quanto tempo o pedido pendente de pagamento deve ser cancelado, define em dias.', 'wc-safrapay_card' ),
					'default'     => __( '0', 'wc-safrapay_card' ),
					'desc_tip'    => true,
				),
				'safrapay_boleto_deadline_boleto' => array(
					'title'       => __( 'Tempo de expiração do boleto (Dias):', 'wc-safrapay-boleto' ),
					'type'        => 'number',
					'description' => __( 'Tempo de expiração do boleto.', 'wc-safrapay-boleto' ),
					'default'     => __( '0', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'safrapay_boleto_fine_start' => array(
					'title'       => __( 'Dias para multa:', 'wc-safrapay-boleto' ),
					'type'        => 'number',
					'description' => __( 'Dias para começar a contar a multa.', 'wc-safrapay-boleto' ),
					'default'     => __( '2', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'safrapay_boleto_fine_value' => array(
					'title'       => __( 'Valor fixo da multa:', 'wc-safrapay-boleto' ),
					'type'        => 'number',
					'description' => __( 'Valor fixo da multa.', 'wc-safrapay-boleto' ),
					'default'     => __( '0', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'safrapay_boleto_fine_percentual' => array(
					'title'       => __( 'Valor percentual da multa:', 'wc-safrapay-boleto' ),
					'type'        => 'number',
					'description' => __( 'valor percentual sobre o valor original da multa.', 'wc-safrapay-boleto' ),
					'default'     => __( '0', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'safrapay_boleto_initial_status'  => array(
					'title'       => __( 'Status do Pedido criado:', 'wc-safrapay-boleto' ),
					'type'        => 'select',
					'options'     => wc_get_order_statuses(),
					'description' => __( 'Status do pedido criado.', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'safrapay_boleto_cnpj'            => array(
					'title'       => __( 'CNPJ:', 'wc-safrapay-boleto' ),
					'type'        => 'text',
					'description' => __( 'Insira o CNPJ cadastrado no Safrapay.', 'wc-safrapay-boleto' ),
					'default'     => __( '', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				),
				'safrapay_boleto_merchantKey'     => array(
					'title'       => __( 'Merchant Token:', 'wc-safrapay-boleto' ),
					'type'        => 'text',
					'description' => __( 'Insira o Merchant Key cadastrado no Safrapay.', 'wc-safrapay-boleto' ),
					'default'     => __( '', 'wc-safrapay-boleto' ),
					'desc_tip'    => true,
				)
			)
		);

	}
	
	/**
	 * Logging method.
	 *
	 * @param string $message Log message.
	 * @param string $level Optional. Default 'info'. Possible values:
	 *                      emergency|alert|critical|error|warning|notice|info|debug.
	 */
	public static function log( $message, $level = 'info' ) {
		if ( self::$log_enabled ) {
			if ( empty( self::$log ) ) {
				self::$log = wc_get_logger();
			}
			self::$log->log( $level, $message, array( 'source' => 'wc-safrapay-boleto' ) );
		}
	}

	/**
	 * Process_payment method.
	 *
	 * @param int $order_id Id of order.
	 */
	public function process_payment( $order_id ) {
		global $woocommerce;
		$order = new WC_Order( $order_id );

		$address_1    = $order->get_meta( '_billing_address_1' );
        $address_2    = $order->get_meta( '_billing_address_2' );
        $address_number =  $order->get_meta( '_billing_number' );
        $address_neightboorhood = $order->get_meta( '_billing_neighborhood' );

		$amount = str_replace( '.', '', $order->get_total() );

		SafrapayPayments\ApiSDK\Configuration::initialize();

		if ( 'sandbox' === $this->environment ) {
			SafrapayPayments\ApiSDK\Configuration::setUrl( SafrapayPayments\ApiSDK\Configuration::DEV_URL );
		}

		$merchant_numeric_cnpj = preg_replace('/[^0-9]/', '', $this->merchant_cnpj);
        SafrapayPayments\ApiSDK\Configuration::setCnpj($merchant_numeric_cnpj);
		SafrapayPayments\ApiSDK\Configuration::setMerchantToken( $this->merchant_key );
		SafrapayPayments\ApiSDK\Configuration::setlog( false );
		SafrapayPayments\ApiSDK\Configuration::login();

		$phone = preg_replace('/[^\d]+/', '', $order->get_billing_phone());

		$customer_phone_area_code = substr( $phone, 0, 2 );
		$customer_phone           = substr( $phone, 2 );

		$gateway = new SafrapayPayments\ApiSDK\Gateway();
		$boleto  = new SafrapayPayments\ApiSDK\Domains\Boleto();

		foreach($order->get_items() as $item) {
			$product = new WC_Product($item->get_product_id());
			$total_product_amount = preg_replace( '/[^0-9]/', '', number_format($item->get_subtotal(),2));
			$product_amount = round($total_product_amount / $item->get_quantity(), 0);
			$boleto->products->add(
				// name
				$item->get_name(),  
				// sku
				$product->get_sku(), 
				// price
				$product_amount, 
				// quantity
				$item->get_quantity()
			);
		}

		$boleto->setDeadline( $this->deadline );

		$boleto->setMerchantChargeId($order_id);

		// ! Customer
		$boleto->customer->setId( "$order_id" );
		$boleto->customer->setName( $order->get_billing_first_name() . ' ' . $order->get_billing_last_name() );
		$boleto->customer->setEmail( $order->get_billing_email() );

		if (!empty($order->get_meta('_billing_cpf'))) {
            $boleto->customer->setDocumentType(SafrapayPayments\ApiSDK\Enum\DocumentType::CPF);
            $documento = preg_replace('/[^\d]+/', '', $order->get_meta('_billing_cpf'));
        } else if (!empty($order->get_meta('_billing_cnpj'))) {
            $boleto->customer->setDocumentType(SafrapayPayments\ApiSDK\Enum\DocumentType::CNPJ);
            $documento = preg_replace('/[^\d]+/', '', $order->get_meta('_billing_cnpj'));
        }
		
		$boleto->customer->setDocument( $documento );
		// ! Customer->address
		$boleto->customer->address->setStreet( $address_1 );
		$boleto->customer->address->setNumber( $address_number );
		$boleto->customer->address->setNeighborhood( $address_neightboorhood );
		$boleto->customer->address->setCity( $order->get_billing_city() );
		$boleto->customer->address->setState( $order->get_billing_state() );
		$boleto->customer->address->setCountry( $order->get_billing_country() );
		$boleto->customer->address->setZipcode( $order->get_billing_postcode() );
		$boleto->customer->address->setComplement( $address_2 );

		// ! Customer->phone
		$boleto->customer->phone->setCountryCode( '55' );
		$boleto->customer->phone->setAreaCode( $customer_phone_area_code );
		$boleto->customer->phone->setNumber( $customer_phone );
		$boleto->customer->phone->setType( SafrapayPayments\ApiSDK\Enum\PhoneType::MOBILE );

		// ! Transactions
		$boleto->transactions->setAmount( $amount );
		$boleto->transactions->setInstructions( 'Crédito de teste' );

		// Transactions->fine (opcional)

		if(!empty($this->get_option('safrapay_boleto_fine_start'))){
			$boleto->transactions->fine->setStartDate($this->fine_start);
			$boleto->transactions->fine->setAmount($this->fine_value);
			$boleto->transactions->fine->setInterest($this->fine_percentual);
		}

		$res = $gateway->charge( $boleto );

		if($this->debug == 'yes') {
			wc_get_logger()->info( wc_print_r( $res, true ), array( 'source' => 'safrapay-boleto-orders-response' ) );
		}

		if ( isset( $res['status'] ) ) {
			if ( SafrapayPayments\ApiSDK\Enum\ChargeStatus::NOT_AUTHORIZED === $res['status'] ) {

				$order->update_status( 'failed', __( 'Pagamento Não Autorizado', 'wc-safrapay-boleto' ) );

				if($this->debug == 'yes') {
					wc_add_notice( json_encode($res) , 'error' );
				}
				wc_add_notice( 'Transação não autorizada.', 'error' );
				return [
					'result'   => 'failure',
					'message'  => __( 'Transação não autorizada.', 'wc-safrapay' )
				];
			}
			else if ( SafrapayPayments\ApiSDK\Enum\ChargeStatus::PRE_AUTHORIZED === $res['status'] ) {

				// ! Insert params to metadata
				$order->update_meta_data(
					'_params_safrapay',
					array(
						'order_id'                       => $order_id,
						'chargeId'                => $res['charge']->id,
						'chargeStatus'            => $res['charge']->chargeStatus,
						'transaction_id'          => $res['charge']->transactions[0]->transactionId,
						'transaction_barcode'     => $res['charge']->transactions[0]->barcode,
						'transaction_digitalLine' => $res['charge']->transactions[0]->digitalLine,
						'transaction_amount'      => $res['charge']->transactions[0]->amount,
						'transaction_transactionStatus' => $res['charge']->transactions[0]->transactionStatus,
						'transaction_bankSlipUrl' => $res['charge']->transactions[0]->bankSlipUrl,
						'transaction_deadline'    => $res['charge']->transactions[0]->deadline,
						'environment'			 => $this->environment
					)
				);

				$order->save();

				$order->update_status( $this->initial_status, __( 'Aguardando o pagamento do boleto', 'wc-safrapay-boleto' ) );
				
				// ! Remove cart
				$woocommerce->cart->empty_cart();
				
				// ! Return thankyou redirect
				return array(
					'result'   => 'success',
					'redirect' => $this->get_return_url( $order ),
				);
			}
			else {				
				$order->update_status( 'failed', __( 'Pagamento Falhou', 'wc-safrapay-boleto' ) );
				if($this->debug == 'yes') {
					return wc_add_notice( json_encode($res) , 'error' );
				}
			}
		} else {
			$order->update_status( 'failed', __( 'Pagamento Falhou', 'wc-safrapay-boleto' ) );
			if ( $res != null ) {
				if($this->debug == 'yes') {
					return wc_add_notice( json_encode($res) , 'error' );
				}
				return wc_add_notice( $res['httpMsg'], 'error' );
			}
		}
	}

		/**
		 * Thankyou_page method.
		 */
	public function thankyou_page() {
		if ( $this->instructions ) {
			echo wp_kses_post( wpautop( wptexturize( $this->instructions ) ) );
		}
	}
}