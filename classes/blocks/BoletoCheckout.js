(function() {
    const boletoSettings = window.wc.wcSettings.getSetting( 'safrapay_boleto_data', {} );
    const label = window.wp.htmlEntities.decodeEntities( boletoSettings.title ) || window.wp.i18n.__( 'SafraPay Boleto', 'safrapay_boleto' );
    const Content = () => {
        return window.wp.htmlEntities.decodeEntities( boletoSettings.description || '' );
    };
    const Block_Gateway = {
        name: 'safrapay_boleto',
        label: label,
        content: Object( window.wp.element.createElement )( Content, null ),
        edit: Object( window.wp.element.createElement )( Content, null ),
        canMakePayment: () => true,
        ariaLabel: label,
        supports: {
            features: boletoSettings.supports,
        },
    };
    window.wc.wcBlocksRegistry.registerPaymentMethod( Block_Gateway );
})();