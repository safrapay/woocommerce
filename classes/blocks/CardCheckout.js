(function() {
    const { createElement, useState, useEffect } = window.wp.element;
    const { TextControl, SelectControl } = window.wp.components;
    const { __ } = window.wp.i18n;

    const cardSettings = window.wc.wcSettings.getSetting('safrapay_card_data', {});
    const label = window.wp.htmlEntities.decodeEntities(cardSettings.title) || __('SafraPay Card', 'safrapay_card');

    const Content = ( props ) => {
        const { eventRegistration, emitResponse } = props;
        const { onPaymentSetup } = eventRegistration;

        const [cardHolderName, setCardHolderName] = useState('');
        const [cardHolderDocument, setCardHolderDocument] = useState('');
        const [cardNumber, setCardNumber] = useState('');
        const [expirationMonth, setExpirationMonth] = useState(getMonthOptions()[0].value);
        const [expirationYear, setExpirationYear] = useState(getYearOptions()[0].value);
        const [cvv, setCvv] = useState('');
        const [installment, setInstallment] = useState('');

        const validateFields = () => {
            // Nome do titular não deve estar vazio
            if (!cardHolderName.trim()) {
                return 'Preencha o nome do titular do cartão.';
            }

            // CPF do titular deve ter 14 caracteres no formato XXX.XXX.XXX-XX
            if (!validateCPF(cardHolderDocument)) {
                return 'Insira um CPF válido no formato 123.456.789-10.';
            }

            // Número do cartão deve estar no formato XXXX XXXX XXXX XXXX ou XXXXXXXXXXXXXXXX
            const cardNumberPattern = /^(\d{4} \d{4} \d{4} \d{4}|\d{16})$/;
            if (!cardNumberPattern.test(cardNumber)) {
                return 'O número do cartão deve estar no formato XXXX XXXX XXXX XXXX ou XXXXXXXXXXXXXXXX.';
            }

            // Mês de expiração deve ser no máximo 12 e ter 2 dígitos
            const month = parseInt(expirationMonth, 10);
            if (!expirationMonth || month < 1 || month > 12) {
                return 'Selecione um mês de expiração válido.';
            }

            // Ano de expiração deve ter 2 dígitos, não pode ser maior que 99, e deve ser maior ou igual ao ano atual
            const fullCurrentYear = new Date().getFullYear();
            const currentYear = fullCurrentYear % 100;
            const year = parseInt(expirationYear, 10);
            if (!expirationYear || year > 99 || year < currentYear) {
                return `O ano de expiração deve estar entre ${currentYear} e 99.`;
            }

            // CVV deve ter exatamente 3 dígitos
            if (!/^\d{3}$/.test(cvv)) {
                return 'O CVV deve ter 3 dígitos.';
            }

            // Parcelamento deve ser um número inteiro maior que 0
            if(!/^\d+$/.test(installment) || parseInt(installment, 10) < 1) {
                return 'Selecione a quantidade de parcelas.';
            }

            return '';
        };

        useEffect( () => {
            const unsubscribe = onPaymentSetup( async () => {
                console.log('Payment processing triggered');
                
                const error = validateFields();
                if (error) {
                    return {
                        type: emitResponse.responseTypes.ERROR,
                        message: error,
                    };
                }

                return {
                    type: emitResponse.responseTypes.SUCCESS,
                    meta: {
                        paymentMethodData: {
                            card_holder_name: cardHolderName,
                            card_holder_document: cardHolderDocument,
                            safrapay_card_number: cardNumber,
                            safrapay_card_expiration_month: expirationMonth,
                            safrapay_card_year_month: expirationYear,
                            safrapay_card_cvv: cvv,
                            safrapay_card_installment: installment
                        },
                    },
                };
            });

            return () => {
                unsubscribe();
            };
        }, [
            emitResponse.responseTypes.ERROR,
            emitResponse.responseTypes.SUCCESS,
            onPaymentSetup,
            cardHolderName,
            cardHolderDocument,
            cardNumber,
            expirationMonth,
            expirationYear,
            cvv,
            installment
        ]);

        return createElement('div', { className: 'safrapay-card-fields' },
            createElement(TextControl, {
                label: __('Nome do Titular do Cartão', 'woocommerce'),
                id: 'card_holder_name',
                value: cardHolderName,
                onChange: setCardHolderName,
                placeholder: __('Ex: João Silva', 'woocommerce'),
                required: true,
            }),
            createElement(TextControl, {
                label: __('CPF do Titular do Cartão', 'woocommerce'),
                id: 'card_holder_document',
                value: cardHolderDocument,
                onChange: setCardHolderDocument,
                placeholder: __('Ex: 123.456.789-10', 'woocommerce'),
                required: true,
            }),
            createElement(TextControl, {
                label: __('Número do Cartão', 'woocommerce'),
                id: 'safrapay_card_number',
                value: cardNumber,
                onChange: setCardNumber,
                placeholder: __('Ex: 1234 5678 9012 3456', 'woocommerce'),
                required: true,
            }),
            createElement('div', { className: 'exp-cvv-container' },
                createElement('div', { className: 'exp-container' },
                    createElement(SelectControl, {
                        label: __('Mês de Expiração', 'woocommerce'),
                        id: 'safrapay_card_expiration_month',
                        value: expirationMonth,
                        options: getMonthOptions(),
                        onChange: setExpirationMonth,
                        required: true,
                    })
                ),
                createElement('div', { className: 'exp-container' },
                    createElement(SelectControl, {
                        label: __('Ano de Expiração', 'woocommerce'),
                        id: 'safrapay_card_expiration_year',
                        value: expirationYear,
                        options: getYearOptions(),
                        onChange: setExpirationYear,
                        required: true,
                    }),
                ),
                createElement(TextControl, {
                    label: __('CVV', 'woocommerce'),
                    id: 'safrapay_card_cvv',
                    value: cvv,
                    onChange: setCvv,
                    placeholder: __('Ex: 123', 'woocommerce'),
                    required: true,
                })
            ),
            createElement(SelectControl, {
                label: __('Quantidade de parcelas', 'woocommerce'),
                id: 'safrapay_card_installment',
                value: installment,
                options: getInstallmentOptions(),
                onChange: setInstallment,
                required: true,
            })
        );
    };

    function getMonthOptions() {
        return Array.from({ length: 12 }, (_, i) => {
            const month = (i + 1).toString().padStart(2, '0');
            return { value: month, label: month };
        });
    }

    function getYearOptions() {
        const fullCurrentYear = new Date().getFullYear();
        const currentYear = fullCurrentYear % 100;
        const years = [];
        for (let i = 0; i <= 15; i++) {
            const year = (currentYear + i).toString();
            years.push({ value: year, label: year });
        }
        return years;
    }

    function getInstallmentOptions() {
        return Object.keys(safrapayCardData.installmentOptions).map(key => ({
            value: key,
            label: safrapayCardData.installmentOptions[key],
        }));
    }

    const Block_Gateway = {
        name: 'safrapay_card',
        label: label,
        content: createElement(Content),
        edit: createElement(Content),
        canMakePayment: () => true,
        ariaLabel: label,
        supports: {
            features: cardSettings.supports,
        },
    };

    window.wc.wcBlocksRegistry.registerPaymentMethod(Block_Gateway);
})();


const validateCPF = (cpf) => {
    cpf = cpf.replace(/\D/g, '');

    if (cpf.length !== 11) {
        return false;
    }

    if (/^(\d)\1+$/.test(cpf)) {
        return false;
    }

    let soma = 0;
    let resto;
    for (let i = 1; i <= 9; i++) {
        soma += parseInt(cpf.substring(i - 1, i)) * (11 - i);
    }
    resto = (soma * 10) % 11;
    if ((resto === 10) || (resto === 11)) {
        resto = 0;
    }
    if (resto !== parseInt(cpf.substring(9, 10))) {
        return false;
    }

    soma = 0;
    for (let i = 1; i <= 10; i++) {
        soma += parseInt(cpf.substring(i - 1, i)) * (12 - i);
    }
    resto = (soma * 10) % 11;
    if ((resto === 10) || (resto === 11)) {
        resto = 0;
    }
    if (resto !== parseInt(cpf.substring(10, 11))) {
        return false;
    }

    return true;
};