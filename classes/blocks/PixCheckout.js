(function() {
    const pixSettings = window.wc.wcSettings.getSetting( 'safrapay_pix_data', {} );
    const label = window.wp.htmlEntities.decodeEntities( pixSettings.title ) || window.wp.i18n.__( 'SafraPay Pix', 'safrapay_pix' );
    const Content = () => {
        return window.wp.htmlEntities.decodeEntities( pixSettings.description || '' );
    };
    const Block_Gateway = {
        name: 'safrapay_pix',
        label: label,
        content: Object( window.wp.element.createElement )( Content, null ),
        edit: Object( window.wp.element.createElement )( Content, null ),
        canMakePayment: () => true,
        ariaLabel: label,
        supports: {
            features: pixSettings.supports,
        },
    };
    window.wc.wcBlocksRegistry.registerPaymentMethod( Block_Gateway );
})();