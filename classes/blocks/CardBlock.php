<?php

use Automattic\WooCommerce\Blocks\Payments\Integrations\AbstractPaymentMethodType;

final class WC_Safrapay_Card_Pay_Gateway_Blocks extends AbstractPaymentMethodType {

    private $gateway;
    protected $name = 'safrapay_card';

    public function initialize() {
        $this->settings = get_option( 'woocommerce_safrapay_card_settings', [] );
        $this->gateway = new WC_Safrapay_Card_Pay_Gateway();
    }

    public function is_active() {
        return $this->gateway->is_available();
    }

    public function get_payment_method_script_handles() {

        wp_register_script(
            'safrapay_card-blocks-integration',
            plugin_dir_url(__FILE__) . 'CardCheckout.js',
            [
                'wc-blocks-registry',
                'wc-blocks-checkout',
                'wc-settings',
                'wp-element',
                'wp-components',
                'wp-html-entities',
                'wp-i18n',
            ],
            null,
            true
        );
        if( function_exists( 'wp_set_script_translations' ) ) {            
            wp_set_script_translations( 'safrapay_card-blocks-integration');
            
        }
        return [ 'safrapay_card-blocks-integration' ];
    }

    public function get_payment_method_data() {
        return [
            'title' => $this->gateway->title
        ];
    }

}
?>